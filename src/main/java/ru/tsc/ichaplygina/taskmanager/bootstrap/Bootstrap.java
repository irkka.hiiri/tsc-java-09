package ru.tsc.ichaplygina.taskmanager.bootstrap;

import ru.tsc.ichaplygina.taskmanager.api.ICommandController;
import ru.tsc.ichaplygina.taskmanager.api.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.api.ICommandService;
import ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst;
import ru.tsc.ichaplygina.taskmanager.constant.CommandConst;
import ru.tsc.ichaplygina.taskmanager.controller.CommandController;
import ru.tsc.ichaplygina.taskmanager.repository.CommandRepository;
import ru.tsc.ichaplygina.taskmanager.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String... args) {
        if (args == null || args.length == 0) processInput();
        else executeCommand(args);
    }

    public void processInput() {
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        String command = readCommand(scanner);
        while (true) {
            executeCommand(command);
            command = readCommand(scanner);
        }
    }

    public void executeCommand(final String command) {
        if (command.isEmpty()) return;
        switch (command) {
            case CommandConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case CommandConst.CMD_HELP:
                commandController.showHelp();
                break;
            case CommandConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.CMD_LIST_COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.CMD_LIST_ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.CMD_EXIT:
                commandController.exit();
            default:
                commandController.showUnknown(command);
        }
    }

    public void executeCommand(final String[] params) {
        if (params == null || params.length == 0) return;
        switch (params[0]) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showUnknown(params[0]);
        }
    }

    public String readCommand(final Scanner scanner) {
        commandController.showCommandPrompt();
        return scanner.nextLine().trim();
    }
}
