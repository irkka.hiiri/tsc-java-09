package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

public interface ICommandController {

    void showHelp();

    void showCommands();

    void showArguments();

    void showUnknown(String arg);

    void exit();

    void showWelcome();

    void showCommandPrompt();

    void showVersion();

    void showAbout();

    void showSystemInfo();
}
