package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
