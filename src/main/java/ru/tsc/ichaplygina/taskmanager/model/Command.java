package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst;
import ru.tsc.ichaplygina.taskmanager.constant.CommandConst;

public class Command {

    private String name = "";

    private String arg = "";

    private String description = "";

    public Command(String name, String description, String arg) {
        this.name = name;
        this.description = description;
        this.arg = arg;
    }

    public Command(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return name + (arg.isEmpty() ? " - " : " [" + arg + "] - ") + description + ".";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
