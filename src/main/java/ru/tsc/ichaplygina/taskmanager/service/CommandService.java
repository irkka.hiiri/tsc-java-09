package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.api.ICommandService;
import ru.tsc.ichaplygina.taskmanager.model.Command;

public final class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }
}
