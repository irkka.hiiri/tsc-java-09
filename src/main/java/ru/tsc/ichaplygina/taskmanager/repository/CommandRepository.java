package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst;
import ru.tsc.ichaplygina.taskmanager.constant.CommandConst;
import ru.tsc.ichaplygina.taskmanager.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(CommandConst.CMD_ABOUT,
            "show developer info",
            ArgumentConst.ARG_ABOUT);

    private static final Command HELP = new Command(CommandConst.CMD_HELP,
            "show this message",
            ArgumentConst.ARG_HELP);

    private static final Command VERSION = new Command(CommandConst.CMD_VERSION,
            "show version info",
            ArgumentConst.ARG_VERSION);

    private static final Command EXIT = new Command(CommandConst.CMD_EXIT,
            "quit");

    private static final Command INFO = new Command(CommandConst.CMD_INFO,
            "show system info",
            ArgumentConst.ARG_INFO);

    private static final Command LIST_COMMANDS = new Command(CommandConst.CMD_LIST_COMMANDS,
            "list available commands");

    private static final Command LIST_ARGUMENTS = new Command(CommandConst.CMD_LIST_ARGUMENTS,
            "list available command-line arguments");

    private static final Command[] COMMANDS = {INFO, ABOUT, VERSION, HELP, LIST_COMMANDS, LIST_ARGUMENTS, EXIT};

    @Override
    public Command[] getCommands() {
        return COMMANDS;
    }

}
