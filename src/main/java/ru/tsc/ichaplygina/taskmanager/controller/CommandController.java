package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.ICommandController;
import ru.tsc.ichaplygina.taskmanager.api.ICommandService;
import ru.tsc.ichaplygina.taskmanager.model.Command;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showWelcome() {
        System.out.println();
        System.out.println("*** WELCOME TO THE ULTIMATE TASK MANAGER ***");
        System.out.println();
        System.out.println("Enter help to show available commands.");
        System.out.println("Enter exit to quit.");
        System.out.println("Enter command:");
        System.out.println();
    }

    @Override
    public void showCommandPrompt() {
        System.out.print("> ");
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int numberOfCpus = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryDisplayed;
        if (maxMemory == Long.MAX_VALUE) maxMemoryDisplayed = "No limit";
        else maxMemoryDisplayed = NumberUtil.convertBytesToString(maxMemory);
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println();
        System.out.println("Available processors: " + numberOfCpus);
        System.out.println("Free memory: " + NumberUtil.convertBytesToString(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryDisplayed);
        System.out.println("Total memory available to JVM: " + NumberUtil.convertBytesToString(totalMemory));
        System.out.println("Memory used by JVM: " + NumberUtil.convertBytesToString(usedMemory));
        System.out.println();
    }

    @Override
    public void showCommands() {
        System.out.println();
        for (final Command command : commandService.getCommands()) {
            if (!command.getName().isEmpty()) System.out.println(command.getName());
        }
        System.out.println();
    }

    @Override
    public void showArguments() {
        System.out.println();
        for (final Command command : commandService.getCommands()) {
            if (!command.getArg().isEmpty()) System.out.println(command.getArg());
        }
        System.out.println();
    }

    @Override
    public void showHelp() {
        System.out.println();
        System.out.println("Run with command-line arguments (as listed in [brackets]).");
        System.out.println("Run with no arguments to enter command-line mode.");
        System.out.println();
        System.out.println("Available commands and arguments:");
        for (final Command command : commandService.getCommands()) System.out.println(command);
        System.out.println();
    }

    @Override
    public void showVersion() {
        System.out.println();
        System.out.println("Version: 0.3.0");
        System.out.println();
    }

    @Override
    public void showAbout() {
        System.out.println();
        System.out.println("Developed by:");
        System.out.println("Irina Chaplygina,");
        System.out.println("Technoserv Consulting,");
        System.out.println("ichaplygina@tsconsulting.com");
        System.out.println();
    }

    @Override
    public void showUnknown(final String arg) {
        System.out.println();
        System.out.println("Unknown argument " + arg);
        System.out.println();
    }

    @Override
    public void exit() {
        System.exit(0);
    }
}
